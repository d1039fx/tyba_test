import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'show_data_type_event.dart';
part 'show_data_type_state.dart';

class ShowDataTypeBloc extends Bloc<ShowDataTypeEvent, ShowDataTypeState> {

  bool showDataType = false;

  ShowDataTypeBloc() : super(const ShowDataTypeInitial(changeList: false)) {
    on<ChangeList>(changeList);
  }

  void changeList(ChangeList event, Emitter<ShowDataTypeState> emitter){
    if(showDataType){
      showDataType = false;
    }else{
      showDataType = true;
    }
    emitter(ShowDataTypeInitial(changeList: showDataType));
  }
}
