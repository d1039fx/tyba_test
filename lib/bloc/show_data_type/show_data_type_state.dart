part of 'show_data_type_bloc.dart';

abstract class ShowDataTypeState extends Equatable {
  final bool changeList;
  const ShowDataTypeState({required this.changeList});
}

class ShowDataTypeInitial extends ShowDataTypeState {
  const ShowDataTypeInitial({required super.changeList});

  @override
  List<Object> get props => [changeList];
}
