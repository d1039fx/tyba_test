part of 'university_data_get_bloc.dart';

abstract class UniversityDataGetState extends Equatable {
  final UniversityModel university;
  const UniversityDataGetState({required this.university});
}

class UniversityDataGetInitial extends UniversityDataGetState {
  const UniversityDataGetInitial({required super.university});

  @override
  List<Object> get props => [university];
}
