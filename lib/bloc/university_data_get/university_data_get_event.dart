part of 'university_data_get_bloc.dart';

abstract class UniversityDataGetEvent extends Equatable {

  final UniversityModel universityModel;

  const UniversityDataGetEvent({required this.universityModel});
}

class GetDataUniversity extends UniversityDataGetEvent{
  const GetDataUniversity({required super.universityModel});

  @override
  // TODO: implement props
  List<Object?> get props => [universityModel];

}
