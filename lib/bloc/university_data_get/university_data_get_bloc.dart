import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_tyba/model/university_model.dart';
import 'package:test_tyba/repository/repository.dart';

part 'university_data_get_event.dart';
part 'university_data_get_state.dart';

class UniversityDataGetBloc
    extends Bloc<UniversityDataGetEvent, UniversityDataGetState> {
  Repository repository = Repository();

  UniversityDataGetBloc()
      : super(const UniversityDataGetInitial(
            university: UniversityModel(
                alpha_two_code: '',
                domains: [],
                country: '',
                web_pages: [],
                name: ''))) {
    on<GetDataUniversity>(getDataUniversity);
  }

  void getDataUniversity(
      GetDataUniversity event, Emitter<UniversityDataGetState> emitter) {
    emitter(UniversityDataGetInitial(university: event.universityModel));
  }
}
