// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'university_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UniversityModel _$$_UniversityModelFromJson(Map<String, dynamic> json) =>
    _$_UniversityModel(
      alpha_two_code: json['alpha_two_code'] as String,
      domains: json['domains'] as List<dynamic>,
      country: json['country'] as String,
      web_pages: json['web_pages'] as List<dynamic>,
      name: json['name'] as String,
    );

Map<String, dynamic> _$$_UniversityModelToJson(_$_UniversityModel instance) =>
    <String, dynamic>{
      'alpha_two_code': instance.alpha_two_code,
      'domains': instance.domains,
      'country': instance.country,
      'web_pages': instance.web_pages,
      'name': instance.name,
    };
