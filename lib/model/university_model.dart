import 'package:freezed_annotation/freezed_annotation.dart';

part 'university_model.freezed.dart';
part 'university_model.g.dart';


@freezed
class UniversityModel with _$UniversityModel {
  const factory UniversityModel(
      {required String alpha_two_code,
      required List domains,
      required String country,
      required List web_pages,
      required String name}) = _UniversityModel;

  factory UniversityModel.fromJson(Map<String, dynamic> json) =>
      _$UniversityModelFromJson(json);
}
