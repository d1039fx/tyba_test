// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'university_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UniversityModel _$UniversityModelFromJson(Map<String, dynamic> json) {
  return _UniversityModel.fromJson(json);
}

/// @nodoc
mixin _$UniversityModel {
  String get alpha_two_code => throw _privateConstructorUsedError;
  List<dynamic> get domains => throw _privateConstructorUsedError;
  String get country => throw _privateConstructorUsedError;
  List<dynamic> get web_pages => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UniversityModelCopyWith<UniversityModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UniversityModelCopyWith<$Res> {
  factory $UniversityModelCopyWith(
          UniversityModel value, $Res Function(UniversityModel) then) =
      _$UniversityModelCopyWithImpl<$Res, UniversityModel>;
  @useResult
  $Res call(
      {String alpha_two_code,
      List<dynamic> domains,
      String country,
      List<dynamic> web_pages,
      String name});
}

/// @nodoc
class _$UniversityModelCopyWithImpl<$Res, $Val extends UniversityModel>
    implements $UniversityModelCopyWith<$Res> {
  _$UniversityModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? alpha_two_code = null,
    Object? domains = null,
    Object? country = null,
    Object? web_pages = null,
    Object? name = null,
  }) {
    return _then(_value.copyWith(
      alpha_two_code: null == alpha_two_code
          ? _value.alpha_two_code
          : alpha_two_code // ignore: cast_nullable_to_non_nullable
              as String,
      domains: null == domains
          ? _value.domains
          : domains // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      country: null == country
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      web_pages: null == web_pages
          ? _value.web_pages
          : web_pages // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UniversityModelCopyWith<$Res>
    implements $UniversityModelCopyWith<$Res> {
  factory _$$_UniversityModelCopyWith(
          _$_UniversityModel value, $Res Function(_$_UniversityModel) then) =
      __$$_UniversityModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String alpha_two_code,
      List<dynamic> domains,
      String country,
      List<dynamic> web_pages,
      String name});
}

/// @nodoc
class __$$_UniversityModelCopyWithImpl<$Res>
    extends _$UniversityModelCopyWithImpl<$Res, _$_UniversityModel>
    implements _$$_UniversityModelCopyWith<$Res> {
  __$$_UniversityModelCopyWithImpl(
      _$_UniversityModel _value, $Res Function(_$_UniversityModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? alpha_two_code = null,
    Object? domains = null,
    Object? country = null,
    Object? web_pages = null,
    Object? name = null,
  }) {
    return _then(_$_UniversityModel(
      alpha_two_code: null == alpha_two_code
          ? _value.alpha_two_code
          : alpha_two_code // ignore: cast_nullable_to_non_nullable
              as String,
      domains: null == domains
          ? _value._domains
          : domains // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      country: null == country
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      web_pages: null == web_pages
          ? _value._web_pages
          : web_pages // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UniversityModel implements _UniversityModel {
  const _$_UniversityModel(
      {required this.alpha_two_code,
      required final List<dynamic> domains,
      required this.country,
      required final List<dynamic> web_pages,
      required this.name})
      : _domains = domains,
        _web_pages = web_pages;

  factory _$_UniversityModel.fromJson(Map<String, dynamic> json) =>
      _$$_UniversityModelFromJson(json);

  @override
  final String alpha_two_code;
  final List<dynamic> _domains;
  @override
  List<dynamic> get domains {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_domains);
  }

  @override
  final String country;
  final List<dynamic> _web_pages;
  @override
  List<dynamic> get web_pages {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_web_pages);
  }

  @override
  final String name;

  @override
  String toString() {
    return 'UniversityModel(alpha_two_code: $alpha_two_code, domains: $domains, country: $country, web_pages: $web_pages, name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UniversityModel &&
            (identical(other.alpha_two_code, alpha_two_code) ||
                other.alpha_two_code == alpha_two_code) &&
            const DeepCollectionEquality().equals(other._domains, _domains) &&
            (identical(other.country, country) || other.country == country) &&
            const DeepCollectionEquality()
                .equals(other._web_pages, _web_pages) &&
            (identical(other.name, name) || other.name == name));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      alpha_two_code,
      const DeepCollectionEquality().hash(_domains),
      country,
      const DeepCollectionEquality().hash(_web_pages),
      name);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UniversityModelCopyWith<_$_UniversityModel> get copyWith =>
      __$$_UniversityModelCopyWithImpl<_$_UniversityModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UniversityModelToJson(
      this,
    );
  }
}

abstract class _UniversityModel implements UniversityModel {
  const factory _UniversityModel(
      {required final String alpha_two_code,
      required final List<dynamic> domains,
      required final String country,
      required final List<dynamic> web_pages,
      required final String name}) = _$_UniversityModel;

  factory _UniversityModel.fromJson(Map<String, dynamic> json) =
      _$_UniversityModel.fromJson;

  @override
  String get alpha_two_code;
  @override
  List<dynamic> get domains;
  @override
  String get country;
  @override
  List<dynamic> get web_pages;
  @override
  String get name;
  @override
  @JsonKey(ignore: true)
  _$$_UniversityModelCopyWith<_$_UniversityModel> get copyWith =>
      throw _privateConstructorUsedError;
}
