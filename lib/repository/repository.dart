import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:test_tyba/model/university_model.dart';

class Repository {
  Future<List<UniversityModel>> getUniversity() async {
    Uri urlData = Uri.parse(
        'https://tyba-assets.s3.amazonaws.com/FE-Engineer-test/universities.json');

    final response = await http.get(urlData);

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body);
      List<Map<String, dynamic>> dataMap =
          data.map<Map<String, dynamic>>((e) => e).toList();
      return dataMap
          .map<UniversityModel>(
              (university) => UniversityModel.fromJson(university))
          .toList();
    } else {
      throw Exception('failed to load data');
    }
  }
}
