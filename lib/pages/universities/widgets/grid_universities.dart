import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc/university_data_get/university_data_get_bloc.dart';
import '../../../model/university_model.dart';
import '../pages_university/pages_university.dart';

class GridUniversities extends StatelessWidget {
  final List<UniversityModel> listUniversities;
  const GridUniversities({Key? key, required this.listUniversities})
      : super(key: key);

  int columnsData(double width) {
    if (width <= 800) {
      return 2;
    } else if (width <= 1000) {
      return 3;
    } else {
      return 4;
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, size) {
      return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: columnsData(size.maxWidth), mainAxisExtent: 100),
        itemBuilder: (context, index) {
          UniversityModel universityModel = listUniversities[index];
          return ListTile(
              title: Text(universityModel.name),
              subtitle: Text(universityModel.web_pages[0]),
              onTap: () {
                context
                    .read<UniversityDataGetBloc>()
                    .add(GetDataUniversity(universityModel: universityModel));
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const PageUniversity()));
              });
        },
        itemCount: listUniversities.length,
      );
    });
  }
}
