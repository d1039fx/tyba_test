import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc/university_data_get/university_data_get_bloc.dart';
import '../../../model/university_model.dart';
import '../pages_university/pages_university.dart';

class ListUniversities extends StatelessWidget {
  final List<UniversityModel> listUniversities;

  const ListUniversities({Key? key, required this.listUniversities})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: listUniversities.length,
        itemBuilder: (context, index) {
          UniversityModel universityModel = listUniversities[index];

          return ListTile(
            title: Text(universityModel.name),
            subtitle: Text(universityModel.web_pages[0]),
            onTap: ()
            {
              context
                  .read<UniversityDataGetBloc>()
                  .add(GetDataUniversity(universityModel: universityModel));
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const PageUniversity()));
            },
          );
        });
  }
}
