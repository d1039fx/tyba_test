import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_tyba/bloc/show_data_type/show_data_type_bloc.dart';
import 'package:test_tyba/model/university_model.dart';
import 'package:test_tyba/pages/universities/widgets/grid_universities.dart';
import 'package:test_tyba/pages/universities/widgets/list_universities.dart';

import '../../repository/repository.dart';

class Universities extends StatelessWidget {
  const Universities({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Universities'),
      ),
      body: FutureBuilder<List<UniversityModel>>(
          future: Repository().getUniversity(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text(snapshot.error.toString()),
              );
            }
            if (snapshot.hasData) {
              return BlocBuilder<ShowDataTypeBloc, ShowDataTypeState>(
                builder: (context, state) {
                  return state.changeList
                      ? GridUniversities(listUniversities: snapshot.requireData)
                      : ListUniversities(
                          listUniversities: snapshot.requireData,
                        );
                },
              );
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          context.read<ShowDataTypeBloc>().add(ChangeList());
        },
        child: CircleAvatar(
          child: BlocBuilder<ShowDataTypeBloc, ShowDataTypeState>(
            builder: (context, state) {
              return state.changeList
                  ? const Icon(Icons.window)
                  : const Icon(Icons.list);
            },
          ),
        ),
      ),
    );
  }
}
