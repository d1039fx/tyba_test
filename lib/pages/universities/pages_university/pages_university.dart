import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_tyba/bloc/university_data_get/university_data_get_bloc.dart';

import 'package:test_tyba/model/university_model.dart';

class PageUniversity extends StatelessWidget {
  const PageUniversity({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UniversityDataGetBloc, UniversityDataGetState>(
      builder: (context, state) {
        UniversityModel universityModel = state.university;
        return Scaffold(
          appBar: AppBar(
            title: Text(universityModel.name),
          ),
          body: Column(
            children: [
              Text(universityModel.name),
              Text(universityModel.country),
              Expanded(
                child: ListView(
                  children: universityModel.web_pages
                      .map<Widget>((e) => Text(e.toString()))
                      .toList(),
                ),
              ),
              Expanded(
                child: ListView(
                  children: universityModel.domains
                      .map<Widget>((e) => Text(e.toString()))
                      .toList(),
                ),
              ),
              Text(universityModel.alpha_two_code)
            ],
          ),
        );
      },
    );
  }
}
