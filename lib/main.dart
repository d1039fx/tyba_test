import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_tyba/bloc/show_data_type/show_data_type_bloc.dart';
import 'package:test_tyba/bloc/university_data_get/university_data_get_bloc.dart';
import 'package:test_tyba/pages/universities/universities.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<UniversityDataGetBloc>(
              create: (context) => UniversityDataGetBloc()),
          BlocProvider<ShowDataTypeBloc>(
              create: (context) => ShowDataTypeBloc()),
        ],
        child: MaterialApp(
          title: 'Tyba Test',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: const Universities(),
        ));
  }
}
